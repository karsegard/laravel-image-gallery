<?php

namespace KDA\ImageGallery;

use KDA\Laravel\PackageServiceProvider;

class ServiceProvider extends PackageServiceProvider
{
    //use \KDA\Laravel\Traits\HasRoutes;
    use \KDA\Laravel\Traits\HasViews;
    use \KDA\Laravel\Traits\HasLoadableMigration;


    protected $viewNamespace = 'kda-gallery';
    protected $publishViewsTo = 'vendor/kda/gallery';

    protected $routes = [
        'gallery.php'
    ];
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
}
