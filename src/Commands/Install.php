<?php

namespace KDA\ImageGallery\Commands;

use GKA\Noctis\Providers\AuthProvider;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Config;

class Install extends Command
{
   
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:gallery:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';


    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
       
        $this->call('vendor:publish', ['--provider' => "Spatie\MediaLibrary\MediaLibraryServiceProvider",'--tag'=>'migrations']);
    }
}
