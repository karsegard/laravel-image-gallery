<?php

namespace KDA\ImageGallery\Models;

use Illuminate\Database\Eloquent\Model;

use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Conversion extends Model 
{
   

    protected $fillable = [
        'key',
    ];
    protected $casts = [
        'id' => 'integer',
        'key' => 'string',
    ];


    public function galleries()
    {
        return $this->belongsToMany(Gallery::class);
    }
    public function formats()
    {
        return $this->belongsToMany(Format::class);
    }

    public function identifiableAttribute(){
        return $this->key;
    }
  
}
