<?php

namespace KDA\ImageGallery\Models;

use Illuminate\Database\Eloquent\Model;

use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Format extends Model 
{
   

    protected $fillable = [
        'desc',
        'operations',
        'conditions',
    ];
    protected $casts = [
        'id' => 'integer',
        'desc' => 'string',
        'operations' => 'array',
        'conditions' => 'array',
    ];


    public function conversions()
    {
        return $this->belongsToMany(Conversion::class);
    }

    public function identifiableAttribute(){
        return $this->desc;
    }
  
}
