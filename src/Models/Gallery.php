<?php

namespace KDA\ImageGallery\Models;

use Illuminate\Database\Eloquent\Model;


class Gallery extends Model 
{
   

    protected $fillable = [
        'name',

    ];
    protected $casts = [
        'id' => 'integer',
        'content' => 'array'
    ];
    public function conversions()
    {
        return $this->belongsToMany(Conversion::class);
    }
    public function albums()
    {
        return $this->hasMany(Album::class);
    }
}
