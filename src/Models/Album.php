<?php

namespace KDA\ImageGallery\Models;

use Illuminate\Database\Eloquent\Model;

use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Image\Image;

class Album extends Model implements HasMedia
{

    use InteractsWithMedia;

    public $registerMediaConversionsUsingModelInstance = true;
    protected $fillable = [
        'name',
        'key',
        'gallery_id',
        'content'
    ];

    protected $casts = [
        'id' => 'integer',
        'content' => 'array'
    ];

    protected $availableConversions = [];


    public function gallery()
    {
        return $this->belongsTo(Gallery::class);
    }

    public function registerMediaConversions($media = null): void
    {
      //  $image = Image::load($media->getPath());

        foreach ($this->gallery->conversions as $conversion) {
      //      $orientation = $image->getHeight() > $image->getWidth() ? 'portrait': 'landscape';
            $key = $conversion->key;
            foreach($conversion->formats as $format){
        //        $match = collect($format->conditions)->where('conditions','orientation')->first();
                
          //      $matchResult =  collect($format->conditions)->where('value',$format['condition'],$orientation)->first();
             //   dump($match,$matchResult);
              
            //    if($match && $matchResult){
                    $cv = $this->addMediaConversion($key);
                    foreach($format->operations as $op){
                        $method = $op['method'];
                        $value = $op['value'];
                        $cv->$method($value);
                    }
             /*   }else if(!$match){
                    $cv = $this->addMediaConversion($key);
                    foreach($format->operations as $op){
                        $method = $op['method'];
                        $value = $op['value'];
                        $cv->$method($value);
                    }
                }
*/
            }
           
        }
     //   dd($this->mediaConversions);
    }

    public function clearImages()
    {
        $this->clearMediaCollection('image');
    }

    public function addImage($file)
    {
        if ($file) {
            $this->addMedia($file)->usingFileName(basename($file))->withCustomProperties(['originalUrl' => $file])->preservingOriginal()->toMediaCollection('image');
        }
    }

    public function getMediaByFile($file){
        
        return $this->getMedia('image',[$file]);
    }

}
