<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConversionFormatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('conversion_format', function (Blueprint $table) {
            $table->id();
            $table->foreignId('format_id')->constrained('formats')->onDelete('restrict');
            $table->foreignId('conversion_id')->constrained('conversions')->onDelete('restrict');

            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('conversion_format');
        Schema::enableForeignKeyConstraints();
    }
}
